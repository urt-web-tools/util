<?php

function send_rcon_cmd($host, $pwd, $cmd) {

if ($pwd != NULL) $cmd = "rcon $pwd $cmd";
$port = 27960;		
$timeout = 15;                               // Default timeout for the php socket (seconds)
$length = 2048;                               // Packet length (should this be larger?)
$magic = "\377\377\377\377";                  // Magic string to send via UDP
$pattern = "/$magic" . "print\n/";
$pattern2 = "/$magic" . "statusResponse\n/";

if(!function_exists("socket_create")) die("<font color=red>socket support missing!</font>");

$socket = socket_create (AF_INET, SOCK_DGRAM, getprotobyname ('udp'));
if (!$socket) die("The server is DOWN!");
if (!socket_set_nonblock ($socket)) die("Error! Unable to set nonblock on socket.");
$time = time();
$error = "";
while (!@socket_connect ($socket, $host, $port )) {	
	$err = socket_last_error ($socket);
	if ($err == 115 || $err == 114) {
		if ((time () - $time) >= $timeout) {
			socket_close ($socket);
			echo "Error! Connection timed out.";
		}
		sleep(1);
		continue;
	}
}

// Verify if an error occured
if( strlen($error) != 0 ) die("Unable to connect to server.");

socket_write ($socket, $magic . "$cmd\n");
$read = array ($socket);
$out = "";
$empty = array();
while (socket_select ($read, $empty, $empty, 1))
{
	$out .= socket_read ($socket, $length, PHP_BINARY_READ);
}

if ($out == "")	die("<center><font color=red><h2>Unable to connect to server...</h2></font></center>\n");

socket_close ($socket);
$out = preg_replace ($pattern, "", $out);
$out = preg_replace ($pattern2, "", $out);
return explode( "\n", $out );
}

?>